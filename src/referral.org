#+TITLE: Referral links
#+INCLUDE: "./common.org"
#+HTML_LINK_UP: index.html
#+OPTIONS: author:nil timestamp:nil

* Crypto.com
[[https://platinum.crypto.com/r/wuwdu28495][https://platinum.crypto.com/r/wuwdu28495]]

When asked, enter referral code: wuwdu28495
* Linode
VPS and other cloud services

[[https://www.linode.com/?r=0a6848992ce7e855b1d6ba205ffe3d9e8d9d2c1e][https://www.linode.com/?r=0a6848992ce7e855b1d6ba205ffe3d9e8d9d2c1e]]
* Odysee on LBRY protocol
[[https://odysse.com/$/invite/@Parklivspodden:7][https://odysse.com/$/invite/@Parklivspodden:7]]
